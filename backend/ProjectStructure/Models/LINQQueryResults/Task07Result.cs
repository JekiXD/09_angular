﻿using System;
using System.Collections.Generic;
using System.Text;
using Models.Models;

namespace Models.LINQQueryResults
{
    public class Task07Result
    {
        public Project Project { get; set; }
        public Task LongestTask { get; set; }
        public Task ShortestTask { get; set; }
        public int? UsersInTeamCount { get; set; }
    }
}

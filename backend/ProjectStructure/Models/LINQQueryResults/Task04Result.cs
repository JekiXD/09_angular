﻿using System;
using System.Collections.Generic;
using System.Text;
using Models.Models;

namespace Models.LINQQueryResults
{
    public class Task04Result
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}

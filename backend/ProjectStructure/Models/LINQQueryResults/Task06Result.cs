﻿using System;
using System.Collections.Generic;
using System.Text;
using Models.Models;

namespace Models.LINQQueryResults
{
    public class Task06Result
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int UnfinishedTasksCount { get; set; }
        public Task LongestTask { get; set; }
    }
}

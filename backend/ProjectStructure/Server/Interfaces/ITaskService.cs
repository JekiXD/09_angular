﻿using Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Interfaces
{
    public interface ITaskService
    {
        Task<IEnumerable<TaskDTO>> GetTasks();
        Task<TaskDTO> GetTaskById(int id);
        Task<TaskDTO> AddTask(TaskDTO task);
        Task UpdateTask(TaskDTO task);
        Task DeleteTask(int id);
        Task<bool> ExistsTask(int id);
    }
}

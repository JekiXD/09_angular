﻿using Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Interfaces
{
    public interface IProjectService
    {
        Task<IEnumerable<ProjectDTO>> GetProjects();
        Task<ProjectDTO> GetProjectById(int id);
        Task<ProjectDTO> AddProject(ProjectDTO project);
        Task UpdateProject(ProjectDTO project);
        Task DeleteProject(int id);
        Task<bool> ExistsProject(int id);
    }
}

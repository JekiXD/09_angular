﻿using Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserDTO>> GetUsers();
        Task<UserDTO> GetUserById(int id);
        Task<IEnumerable<TaskDTO>> GetUnfinishedTasks(int userId);
        Task<UserDTO> AddUser(UserDTO user);
        Task UpdateUser(UserDTO user);
        Task DeleteUser(int id);
        Task<bool> ExistsUser(int id);
    }
}

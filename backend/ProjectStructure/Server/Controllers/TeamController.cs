﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Services;
using Models.DTO;
using System.Net;
using Server.Interfaces;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        readonly ITeamService _service;
        public TeamController(ITeamService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("read")]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> Read()
        {
            return Ok(await _service.GetTeams());
        }

        [HttpGet]
        [Route("read/{id}")]
        public async Task<ActionResult<TeamDTO>> Read(int id)
        {
            TeamDTO team = await _service.GetTeamById(id);

            if (team == null) return NotFound();
            return Ok(team);
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> Create([FromBody] TeamDTO team)
        {
            if (await _service.ExistsTeam(team.Id)) return Conflict("Such team already exists");
            var newTeam = await _service.AddTeam(team);
            return StatusCode((int)HttpStatusCode.Created, newTeam);
        }

        [HttpPut]
        [Route("update")]
        public async Task<IActionResult> Update([FromBody] TeamDTO team)
        {
            await _service.UpdateTeam(team);
            return NoContent();
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!await _service.ExistsTeam(id)) return NotFound();
            await _service.DeleteTeam(id);
            return NoContent();
        }
    }
}

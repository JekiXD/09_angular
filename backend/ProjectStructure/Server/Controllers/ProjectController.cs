﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Services;
using Models.DTO;
using System.Net;
using Server.Interfaces;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        readonly IProjectService _service;
        public ProjectController(IProjectService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("read")]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> Read()
        {
            return Ok(await _service.GetProjects());
        }

        [HttpGet]
        [Route("read/{id}")]
        public async Task<ActionResult<ProjectDTO>> Read(int id)
        {
            ProjectDTO project = await _service.GetProjectById(id);

            if (project == null) return NotFound();
            return Ok(project);
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> Create([FromBody] ProjectDTO project)
        {
            if (await _service.ExistsProject(project.Id)) return Conflict("Entity already exists");
            var createdProject = _service.AddProject(project);
            return StatusCode((int)HttpStatusCode.Created, createdProject);
        }

        [HttpPut]
        [Route("update")]
        public async Task<IActionResult> Update([FromBody] ProjectDTO project)
        {
            await _service.UpdateProject(project);
            return NoContent();
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!await _service.ExistsProject(id)) return NotFound();
            await _service.DeleteProject(id);
            return NoContent();
        }
    }
}

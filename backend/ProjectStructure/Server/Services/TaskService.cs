﻿using AutoMapper;
using MM = Models.Models;
using Models.DTO;
using Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class TaskService : ITaskService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IRepository<MM.Project> _projectRepository;
        readonly IRepository<MM.Task> _taskRepository;
        readonly IRepository<MM.User> _userRepository;
        readonly IMapper _mapper;
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _projectRepository = _unitOfWork.Set<MM.Project>();
            _taskRepository = _unitOfWork.Set<MM.Task>();
            _userRepository = _unitOfWork.Set<MM.User>();
            _mapper = mapper;
        }

        public async Task<IEnumerable<TaskDTO>> GetTasks()
        {
            var taskSet = await _taskRepository.Get();

            return _mapper.Map<IEnumerable<TaskDTO>>(taskSet);
        }

        public async Task<TaskDTO> GetTaskById(int id)
        {
            var taskSet = await _taskRepository.Get(t => t.Id == id);

            if (!taskSet.Any()) return null;

            return _mapper.Map<TaskDTO>(taskSet.First());
        }

        public async Task<TaskDTO> AddTask(TaskDTO task)
        {
            MM.Task newTask = _mapper.Map<MM.Task>(task);

            var taskPerformer = await getTaskPerformer(task);
            var project = await getTaskProject(task);

            newTask.Performer = taskPerformer;
            newTask.Project = project;

            await _taskRepository.Create(newTask);
            await _unitOfWork.SaveChangesAsync();

            return _mapper.Map<TaskDTO>(newTask);
        }

        public async Task UpdateTask(TaskDTO task)
        {
            MM.Task newTask = _mapper.Map<MM.Task>(task);

            var taskPerformer = await getTaskPerformer(task);
            var project = await getTaskProject(task);

            newTask.Performer = taskPerformer;
            newTask.Project = project;

            await _taskRepository.Update(newTask);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteTask(int id)
        {
            await _taskRepository.Delete(id);
            await _unitOfWork.SaveChangesAsync();
        }

        async Task<MM.User> getTaskPerformer(TaskDTO task)
        {
            var userSet = await _userRepository.Get(u => u.Id == task.PerformerId);

            return userSet.FirstOrDefault();
        }

        async Task<MM.Project> getTaskProject(TaskDTO task)
        {
            var projectSet = await _projectRepository.Get(p => p.Id == task.ProjectId);

            return projectSet.FirstOrDefault();
        }

        public async Task<bool> ExistsTask(int id)
        {
            var tasks = await _taskRepository.Get(t => t.Id == id);
            return tasks.Any();
        }
    }
}

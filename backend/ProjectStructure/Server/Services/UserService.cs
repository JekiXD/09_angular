﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Repositories;
using Server.Interfaces;
using Models.DTO;
using AutoMapper;
using MM = Models.Models;

namespace Server.Services
{
    public class UserService : IUserService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IRepository<MM.User> _userRepository;
        readonly IRepository<MM.Team> _teamRepository;
        readonly IRepository<MM.Task> _taskRepository;
        readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _userRepository = _unitOfWork.Set<MM.User>();
            _teamRepository = _unitOfWork.Set<MM.Team>();
            _taskRepository = _unitOfWork.Set<MM.Task>();
            _mapper = mapper;
        }

        public async Task<IEnumerable<UserDTO>> GetUsers()
        {
            var userSet = await _userRepository.Get();

            return _mapper.Map<IEnumerable<UserDTO>>(userSet);
        }

        public async Task<UserDTO> GetUserById(int id)
        {
            var userSet = await _userRepository.Get(u => u.Id == id);

            if (!userSet.Any()) return null;

            return _mapper.Map<UserDTO>(userSet.First());
        }

        public async Task<IEnumerable<TaskDTO>> GetUnfinishedTasks(int userId)
        {
            var taskSet = await _taskRepository.Get(t => t.Performer.Id == userId
                                               && !t.FinishedAt.HasValue);

            return _mapper.Map<IEnumerable<TaskDTO>>(taskSet);
        }

        public async Task<UserDTO> AddUser(UserDTO user)
        {
            MM.User newUser = _mapper.Map<MM.User>(user);
            var team = await getUserTeam(user);

            newUser.Team = team;

            await _userRepository.Create(newUser);
            await _unitOfWork.SaveChangesAsync();

            return _mapper.Map<UserDTO>(newUser);
        }

        public async Task UpdateUser(UserDTO user)
        {
            MM.User newUser = _mapper.Map<MM.User>(user);
            var team = await getUserTeam(user);

            newUser.Team = team;

            await _userRepository.Update(newUser);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteUser(int id)
        {
            await _userRepository.Delete(id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<MM.Team> getUserTeam(UserDTO user)
        {
            var team = await _teamRepository.Get(t => t.Id == user.TeamId);

            return team.FirstOrDefault();
        }

        public async Task<bool> ExistsUser(int id)
        {
            var users = await _userRepository.Get(u => u.Id == id);
            return users.Any();
        }
    }
}

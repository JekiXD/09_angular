﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class AddLogoToProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Logo",
                table: "Projects",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(4029), new DateTime(2021, 12, 10, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(5049) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(5307), new DateTime(2021, 10, 1, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(5319) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(5534));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6366));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6377));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6380));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6382));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6387));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6389));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6391));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6394));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 148, DateTimeKind.Local).AddTicks(4748));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(2271));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3200), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(2645), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(2645) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3470), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3460), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3460) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3475), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3473), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3473) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3479), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3477), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3477) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3483), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3481), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3481) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3490), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3488), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3488) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Logo",
                table: "Projects");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1966), new DateTime(2021, 12, 10, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(2970) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(3217), new DateTime(2021, 10, 1, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(3228) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(3437));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4249));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4259));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4261));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4264));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4268));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4271));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4273));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4275));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 57, 2, 890, DateTimeKind.Local).AddTicks(3494));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(198));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1111), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(565), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(565) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1375), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1366), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1366) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1380), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1378), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1378) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1384), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1382), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1382) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1388), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1386), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1386) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1425), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1423), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1423) });
        }
    }
}

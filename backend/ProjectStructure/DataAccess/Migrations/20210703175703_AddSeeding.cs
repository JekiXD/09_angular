﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class AddSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 1, new DateTime(2021, 7, 3, 20, 57, 2, 890, DateTimeKind.Local).AddTicks(3494), "FisrtTeam" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 2, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(198), "SecondTeam" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "CreatedAt", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { 3, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1380), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1378), null, "Daniel", null, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1378), null });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "CreatedAt", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1111), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(565), null, "Jake", null, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(565), 1 },
                    { 4, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1384), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1382), null, "Sasuke", null, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1382), 1 },
                    { 2, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1375), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1366), null, "Anna", null, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1366), 2 },
                    { 5, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1388), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1386), null, "Megumi", null, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1386), 2 },
                    { 6, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1425), new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1423), null, "Caron", null, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1423), 2 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, 4, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(3217), new DateTime(2021, 10, 1, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(3228), null, "SecondProject", 1 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, 2, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(1966), new DateTime(2021, 12, 10, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(2970), null, "FirstProject", 2 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 6, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4268), null, null, "SixthTask", 1, 2, 2 },
                    { 7, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4271), null, null, "SeventhTask", 4, 2, 2 },
                    { 8, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4273), null, null, "EighthTask", 4, 2, 2 },
                    { 9, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4275), null, null, "NinthTask", 1, 2, 2 },
                    { 1, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(3437), null, null, "FirstTask", 6, 1, 2 },
                    { 2, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4249), null, null, "SecondTask", 6, 1, 2 },
                    { 3, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4259), null, null, "ThirdTask", 2, 1, 2 },
                    { 4, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4261), null, null, "FourthTask", 2, 1, 2 },
                    { 5, new DateTime(2021, 7, 3, 20, 57, 2, 892, DateTimeKind.Local).AddTicks(4264), null, null, "FifthTask", 5, 1, 2 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}

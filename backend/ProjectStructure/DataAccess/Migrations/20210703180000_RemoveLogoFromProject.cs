﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class RemoveLogoFromProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Logo",
                table: "Projects");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6721), new DateTime(2021, 12, 10, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(7698) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(7938), new DateTime(2021, 10, 1, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(7949) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(8152));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(8949));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(8959));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(8962));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(8964));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(8969));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(8971));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(8973));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(8976));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 21, 0, 0, 346, DateTimeKind.Local).AddTicks(8630));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(5070));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(5951), new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(5424), new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(5424) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6209), new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6199), new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6199) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6213), new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6211), new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6211) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6217), new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6215), new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6215) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6221), new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6219), new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6219) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6227), new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6225), new DateTime(2021, 7, 3, 21, 0, 0, 348, DateTimeKind.Local).AddTicks(6225) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Logo",
                table: "Projects",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(4029), new DateTime(2021, 12, 10, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(5049) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(5307), new DateTime(2021, 10, 1, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(5319) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(5534));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6366));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6377));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6380));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6382));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6387));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6389));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6391));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(6394));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 148, DateTimeKind.Local).AddTicks(4748));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(2271));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3200), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(2645), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(2645) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3470), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3460), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3460) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3475), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3473), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3473) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3479), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3477), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3477) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3483), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3481), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3481) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "CreatedAt", "RegisteredAt" },
                values: new object[] { new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3490), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3488), new DateTime(2021, 7, 3, 20, 58, 57, 150, DateTimeKind.Local).AddTicks(3488) });
        }
    }
}

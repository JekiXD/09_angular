﻿using System;
using System.Net.Http;
using Newtonsoft.Json;
using Models.Models;
using Models.DTO;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;

namespace _01_linq
{
    public static class Server
    {
        static HttpClient _client = new HttpClient();
        static string uri = "https://localhost:5001/";
        public static async System.Threading.Tasks.Task<List<Project>> GetHierarchy()
        {
            var taskListTask = GetTasks();
            var projectListTask = GetProjects();
            var teamListTask = GetTeams();
            var userListTask = GetUsers();

            await System.Threading.Tasks.Task.WhenAll(taskListTask,
                                                      projectListTask,
                                                      teamListTask,
                                                      userListTask);

            List<TaskDTO> taskList = await taskListTask;
            List<ProjectDTO> projectList = await projectListTask;
            List<TeamDTO> teamList = await teamListTask;
            List<UserDTO> userList = await userListTask;

            List<Project> results = projectList
                                            .GroupJoin(
                                                taskList.GroupJoin(
                                                            userList,
                                                            task => task.PerformerId,
                                                            performer => performer.Id,
                                                            (task, performer) => new { task, performer })
                                                        .SelectMany(
                                                            res => res.performer.DefaultIfEmpty(),
                                                            (task, performer) => new { task.task, performer })
                                                        .Select(res => new
                                                        {
                                                            task = new Task
                                                            {
                                                                Id = res.task.Id,
                                                                Name = res.task.Name,
                                                                Description = res.task.Description,
                                                                State = res.task.State,
                                                                CreatedAt = res.task.CreatedAt,
                                                                FinishedAt = res.task.FinishedAt,
                                                                Performer = res.performer == null ? null :
                                                                    new User
                                                                    {
                                                                        Id = res.performer.Id,
                                                                        TeamId = res.performer.TeamId,
                                                                        FirstName = res.performer.FirstName,
                                                                        LastName = res.performer.LastName,
                                                                        Email = res.performer.Email,
                                                                        RegisteredAt = res.performer.RegisteredAt,
                                                                        BirthDay = res.performer.BirthDay
                                                                    }
                                                            },
                                                            ProjectId = res.task.ProjectId
                                                        }),
                                                project => project.Id,
                                                task => task.ProjectId,
                                                (p, tasks) => new { project = p, tasks = tasks.Select(t => t.task).ToList() })
                                            .GroupJoin(
                                                teamList,
                                                project => project.project.TeamId,
                                                team => team.Id,
                                                (p, t) => new { p.project, p.tasks, teams = t })
                                            .SelectMany(
                                                res => res.teams.DefaultIfEmpty(),
                                                (project, team) => new { project.project, project.tasks, team })
                                            .Select(
                                                p => new { p.project, p.tasks, team = p.team })
                                            .Join(
                                                userList,
                                                project => project.project.AuthorId,
                                                user => user.Id,
                                                (p, user) => new Project
                                                {
                                                    Id = p.project.Id,
                                                    Author = new User
                                                    {
                                                        Id = user.Id,
                                                        TeamId = user.TeamId,
                                                        FirstName = user.FirstName,
                                                        LastName = user.LastName,
                                                        Email = user.Email,
                                                        RegisteredAt = user.RegisteredAt,
                                                        BirthDay = user.BirthDay
                                                    },
                                                    Team = p.team == null ? null :
                                                    new Team
                                                    {
                                                        Id = p.team.Id,
                                                        Name = p.team.Name,
                                                        CreatedAt = p.team.CreatedAt
                                                    },
                                                    Tasks = p.tasks,
                                                    Name = p.project.Name,
                                                    Description = p.project.Description,
                                                    Deadline = p.project.Deadline
                                                })
                                            .ToList();

            return results;
        }

        public static async System.Threading.Tasks.Task<List<TaskDTO>> GetTasks()
        {
            string data = await _client.GetStringAsync(uri + "api/task/read");
            List<TaskDTO> taskList = JsonConvert.DeserializeObject<List<TaskDTO>>(data);
            return taskList;
        }

        public static async System.Threading.Tasks.Task<List<ProjectDTO>> GetProjects()
        {
            string data = await _client.GetStringAsync(uri + "api/project/read");
            List<ProjectDTO> projectList = JsonConvert.DeserializeObject<List<ProjectDTO>>(data);
            return projectList;
        }

        public static async System.Threading.Tasks.Task<List<TeamDTO>> GetTeams()
        {
            string data = await _client.GetStringAsync(uri + "api/team/read");
            List<TeamDTO> teamList = JsonConvert.DeserializeObject<List<TeamDTO>>(data);
            return teamList;
        }

        public static async System.Threading.Tasks.Task<List<UserDTO>> GetUsers()
        {
            string data = await _client.GetStringAsync(uri + "api/user/read");
            List<UserDTO> userList = JsonConvert.DeserializeObject<List<UserDTO>>(data);
            return userList;
        }

        public static async System.Threading.Tasks.Task<HttpResponseMessage> TasksUpdate(TaskDTO task)
        {
            string jsonString = JsonConvert.SerializeObject(task);
            var response = await _client.PutAsync(uri + "api/task/update", new StringContent(jsonString, Encoding.UTF8, "application/json"));
            return response;
        }
    }
}

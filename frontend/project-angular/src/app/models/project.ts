import { Data } from "@angular/router";


export interface Project{
    id: number;
    authorId: number;
    teamId: number;
    name: string;
    description: string;
    createdAt: Date;
    deadline: Date;
}
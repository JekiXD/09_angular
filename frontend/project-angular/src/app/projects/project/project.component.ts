import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Project } from 'src/app/models/project';
import { ProjectService } from '../services/project.service';
import { catchError } from 'rxjs/operators';
import { ComponentCanDeactivate } from 'src/app/interfaces/ComponentCanDeactivate';
import * as _ from 'lodash';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit, ComponentCanDeactivate {
  public displayAction = "update";
  public projects: Project[] = [];
  public updateProject: Project = {} as Project;
  public deleteId: number = 1;

  constructor(private service: ProjectService) { }

  ngOnInit(): void {
    this.getProjects();
  }

  public getProjects(): void
  {
    this.service.getProjects()
    .pipe(catchError(this.handleError))
    .subscribe(
      (p) => {
        this.projects = p;
        if(p.length > 0) this.updateProject = Object.assign({}, p[0]);;
      },
      (error) => console.log(error));
  }

  public putProject(): void{
    this.service.updateProject(this.updateProject)
    .pipe(catchError(this.handleError))
    .subscribe(_ => this.getProjects());
  }

  public deleteProject(): void{
    this.service.deleteProject(this.deleteId)
    .pipe(catchError(this.handleError))
    .subscribe(_ => this.getProjects());
  }

  public show(action: string): void
  {
    this.displayAction = action;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) console.error('An error occurred:', error.error);
    else console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    return throwError(
      'Something bad happened; please try again later.');
  }

  public loadProject()
  {
    const project = this.projects.find(p => p.id === this.updateProject.id);
    if(project !== undefined) this.updateProject = Object.assign({}, project);
  }

  public canDeactivate() : boolean | Observable<boolean>{
     
    if(this.projects.length > 0 && !_.isEqual(this.updateProject, this.projects[0])){
        return confirm("Вы хотите покинуть страницу?");
    }
    else{
        return true;
    }
  }
}

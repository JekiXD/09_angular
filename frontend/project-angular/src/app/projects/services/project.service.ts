import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Project } from 'src/app/models/project';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  public baseUrl: string = environment.apiUrl;
  public headers = new HttpHeaders();

  constructor(private http: HttpClient) { }

  public getProjects() {
    return this.http.get<Project[]>(`${this.baseUrl}/api/project/read`);
  }

  public updateProject(project: Project) {
    return this.http.put<Project>(`${this.baseUrl}/api/project/update`, project as object);
  }

  public deleteProject(id: number) {
    return this.http.delete<Project>(`${this.baseUrl}/api/project/delete/${id}`);
  }
}

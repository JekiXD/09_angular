import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectComponent } from './project/project.component';
import { ProjectService } from './services/project.service';
import { AppModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { MyDateFormatPipe } from '../pipes/my-date-format.pipe';
import { SharingModule } from '../sharing/sharing.module';



@NgModule({
  declarations: [
    ProjectComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    SharingModule
  ],
  exports: [
    ProjectComponent
  ],
  providers: [
    ProjectService
  ]
})
export class ProjectsModule { }

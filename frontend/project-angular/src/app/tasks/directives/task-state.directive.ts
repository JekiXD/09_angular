import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appTaskState]'
})
export class TaskStateDirective {

  constructor(private el: ElementRef) {  }

  @Input() 
  set appTaskState(value: number)
  {
    switch(value)
    {
      case 1: //New
        this.el.nativeElement.style.background = "#ffffff";
        break;
      case 2: //FInished
        this.el.nativeElement.style.background = "#93c47d";
        break;
      case 3: //Active
        this.el.nativeElement.style.background = "#9fc5f8";
        break;
      case 4: //Terminated
        this.el.nativeElement.style.background = "#e06666";
        break;
    }
  }
}

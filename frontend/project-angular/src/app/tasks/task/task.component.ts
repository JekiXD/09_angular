import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { TaskService } from '../services/task.service';
import { Task as TaskModel } from 'src/app/models/task';
import { ComponentCanDeactivate } from 'src/app/interfaces/ComponentCanDeactivate';
import * as _ from 'lodash';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit, ComponentCanDeactivate {
  public displayAction = "update";
  public tasks: TaskModel[] = [];
  public updateTask: TaskModel = {} as TaskModel;
  public deleteId: number = 1;

  constructor(private service: TaskService) { }

  ngOnInit(): void {
    this.getTasks();
  }

  public getTasks(): void
  {
    this.service.getTasks()
    .pipe(catchError(this.handleError))
    .subscribe(
      (t) => {
        this.tasks = t;
        if(t.length > 0) this.updateTask = Object.assign({}, t[0]);;
      },
      (error) => console.log(error));
  }

  public putTask(): void{
    this.service.updateTask(this.updateTask)
    .pipe(catchError(this.handleError))
    .subscribe(_ => this.getTasks());
  }

  public deleteTask(): void{
    this.service.deleteTask(this.deleteId)
    .pipe(catchError(this.handleError))
    .subscribe(_ => this.getTasks());
  }

  public show(action: string): void
  {
    this.displayAction = action;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) console.error('An error occurred:', error.error);
    else console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    return throwError(
      'Something bad happened; please try again later.');
  }

  public loadTask()
  {
    const task = this.tasks.find(t => t.id === this.updateTask.id);
    if(task !== undefined) this.updateTask = Object.assign({}, task);
  }

  public canDeactivate() : boolean | Observable<boolean>{
     
    if(this.tasks.length > 0 && !_.isEqual(this.updateTask, this.tasks[0])){
        return confirm("Вы хотите покинуть страницу?");
    }
    else{
        return true;
    }
  }
}

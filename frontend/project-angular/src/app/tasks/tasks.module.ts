import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskComponent } from './task/task.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { TaskService } from './services/task.service';
import { SharingModule } from '../sharing/sharing.module';
import { TaskStateDirective } from './directives/task-state.directive';



@NgModule({
  declarations: [
    TaskComponent,
    TaskStateDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    SharingModule   
  ],
  exports: [
    TaskComponent
  ],
  providers: [
    TaskService
  ]
})
export class TasksModule { }

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Task as TaskModel } from 'src/app/models/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  public baseUrl: string = environment.apiUrl;
  public headers = new HttpHeaders();

  constructor(private http: HttpClient) { }

  public getTasks() {
    return this.http.get<TaskModel[]>(`${this.baseUrl}/api/task/read`);
  }

  public updateTask(task: TaskModel) {
    return this.http.put<TaskModel>(`${this.baseUrl}/api/task/update`, task);
  }

  public deleteTask(id: number) {
    return this.http.delete<TaskModel>(`${this.baseUrl}/api/task/delete/${id}`);
  }
}

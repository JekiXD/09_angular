import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ComponentCanDeactivate } from 'src/app/interfaces/ComponentCanDeactivate';
import { User } from 'src/app/models/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, ComponentCanDeactivate {
  public displayAction = "update";
  public users: User[] = [];
  public updateUser: User = {} as User;
  public deleteId: number = 1;

  constructor(private service: UserService) { }

  ngOnInit(): void {
    this.getUsers();
  }

  public getUsers(): void
  {
    this.service.getUsers()
    .pipe(catchError(this.handleError))
    .subscribe(
      (u) => {
        this.users = u;
        if(u.length > 0) this.updateUser = Object.assign({}, u[0]);;
      },
      (error) => console.log(error));
  }

  public putUser(): void{
    this.service.updateUser(this.updateUser)
    .pipe(catchError(this.handleError))
    .subscribe(_ => this.getUsers());
  }

  public deleteUser(): void{
    this.service.deleteUser(this.deleteId)
    .pipe(catchError(this.handleError))
    .subscribe(_ => this.getUsers());
  }

  public show(action: string): void
  {
    this.displayAction = action;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) console.error('An error occurred:', error.error);
    else console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    return throwError(
      'Something bad happened; please try again later.');
  }

  public loadUser()
  {
    const user = this.users.find(p => p.id === this.updateUser.id);
    if(user !== undefined) this.updateUser = Object.assign({}, user);
  }

  public canDeactivate() : boolean | Observable<boolean>{
     
    if(this.users.length > 0 && !_.isEqual(this.updateUser, this.users[0])){
        return confirm("Вы хотите покинуть страницу?");
    }
    else{
        return true;
    }
  }
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'src/app/models/user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public baseUrl: string = environment.apiUrl;
  public headers = new HttpHeaders();

  constructor(private http: HttpClient) { }

  public getUsers() {
    return this.http.get<User[]>(`${this.baseUrl}/api/user/read`);
  }

  public updateUser(user: User) {
    return this.http.put<User>(`${this.baseUrl}/api/user/update`, user);
  }

  public deleteUser(id: number) {
    return this.http.delete<User>(`${this.baseUrl}/api/user/delete/${id}`);
  }
}

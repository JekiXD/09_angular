import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExitAboutGuard } from './guards/exit-about.guard';
import { ProjectComponent } from './projects/project/project.component';
import { TaskComponent } from './tasks/task/task.component';
import { TeamComponent } from './teams/team/team.component';
import { UserComponent } from './users/user/user.component';

const routes: Routes = [
  { path: 'projects', component: ProjectComponent, canDeactivate: [ExitAboutGuard] },
  { path: 'tasks', component: TaskComponent, canDeactivate: [ExitAboutGuard] },
  { path: 'users', component: UserComponent, canDeactivate: [ExitAboutGuard] },
  { path: 'teams', component: TeamComponent, canDeactivate: [ExitAboutGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

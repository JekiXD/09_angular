import { Pipe, PipeTransform } from '@angular/core';
import { monthNames } from '../constants/MonthNames';

@Pipe({
  name: 'myDateFormat'
})
export class MyDateFormatPipe implements PipeTransform {

  transform(value: Date, ...args: unknown[]): string {
    if(value)
    {
      let newValue = new Date(value);
      const date = newValue.getDate();
      const year = newValue.getFullYear();
      const monthName = monthNames[newValue.getMonth()];

      return `${date} ${monthName.toLowerCase()} ${year}`;
    }
    return '';
  }
}

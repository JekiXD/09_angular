import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Team } from 'src/app/models/team';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  public baseUrl: string = environment.apiUrl;
  public headers = new HttpHeaders();

  constructor(private http: HttpClient) { }

  public getTeams() {
    return this.http.get<Team[]>(`${this.baseUrl}/api/team/read`);
  }

  public updateTeam(team: Team) {
    return this.http.put<Team>(`${this.baseUrl}/api/team/update`, team);
  }

  public deleteTeam(id: number) {
    return this.http.delete<Team>(`${this.baseUrl}/api/team/delete/${id}`);
  }
}

import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ComponentCanDeactivate } from 'src/app/interfaces/ComponentCanDeactivate';
import { Team } from 'src/app/models/team';
import { TeamService } from '../services/team.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit, ComponentCanDeactivate {

  public displayAction = "update";
  public teams: Team[] = [];
  public updateTeam: Team = {} as Team;
  public deleteId: number = 1;

  constructor(private service: TeamService) { }
  ngOnInit(): void {
    this.getTeams();
  }

  public getTeams(): void
  {
    this.service.getTeams()
    .pipe(catchError(this.handleError))
    .subscribe(
      (t) => {
        this.teams = t;
        if(t.length > 0) this.updateTeam = Object.assign({}, t[0]);
      },
      (error) => console.log(error));
  }

  public putTeam(): void{
    this.service.updateTeam(this.updateTeam)
    .pipe(catchError(this.handleError))
    .subscribe(_ => this.getTeams());
  }

  public deleteTeam(): void{
    this.service.deleteTeam(this.deleteId)
    .pipe(catchError(this.handleError))
    .subscribe(_ => this.getTeams());
  }

  public show(action: string): void
  {
    this.displayAction = action;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) console.error('An error occurred:', error.error);
    else console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    return throwError(
      'Something bad happened; please try again later.');
  }

  public loadTeam()
  {
    const team = this.teams.find(t => t.id === this.updateTeam.id);
    if(team !== undefined) this.updateTeam = Object.assign({}, team);
  }

  public canDeactivate() : boolean | Observable<boolean>{
     
    if(this.teams.length > 0 && !_.isEqual(this.updateTeam, this.teams[0])){
        return confirm("Вы хотите покинуть страницу?");
    }
    else{
        return true;
    }
  }
}

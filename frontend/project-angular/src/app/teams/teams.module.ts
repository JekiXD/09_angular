import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamComponent } from './team/team.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { TeamService } from './services/team.service';
import { SharingModule } from '../sharing/sharing.module';



@NgModule({
  declarations: [
    TeamComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    SharingModule
  ],
  exports: [
    TeamComponent
  ],
  providers: [
    TeamService
  ]
})
export class TeamsModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyDateFormatPipe } from '../pipes/my-date-format.pipe';



@NgModule({
  declarations: [
    MyDateFormatPipe
  ],
  imports: [
    CommonModule
  ],
  exports:[
    MyDateFormatPipe
  ],
})
export class SharingModule { }
